/*!
 * q - http
 * Copyright (c) 2011 LearnBoost <tj@learnboost.com>
 * MIT Licensed
 *
 * Updated to include WorkHound authentication by workhoundapp.com
 */

/**
 * Module dependencies.
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const provides = require('./middleware/provides');
const stylus = require('stylus');
const routes = require('./routes');
const pug = require('pug');
const json = require('./routes/json');
const util = require('util');
const nib = require('nib');
const enforce = require('express-sslify');
const env = process.env.NODE_ENV || 'development';

// expose the app
module.exports = app;

// stylus config
function compile( str, path ) { // eslint-disable-line
  return stylus(str)
    .set('filename', path)
    .use(nib());
}

// config
app.set('view options', {doctype: 'html'});
app.set('view engine', 'pug');
app.engine('pug', pug.renderFile);
app.set('views', __dirname + '/views');
app.set('title', 'Kue');
app.locals = {inspect: util.inspect};

// middlewares
app.use(stylus.middleware({src: __dirname + '/public', compile: compile}));
app.use(express.static(__dirname + '/public'));

// SSL
if(env != 'development') {
  console.log('using SSL');
// express-sslify: Use enforce.HTTPS({ trustProtoHeader: true }) in case you are
// behind a load balancer (e.g. Heroku).
  app.use(enforce.HTTPS({trustProtoHeader: true})); // eslint-disable-line
}

// JSON api
app.get('/stats', provides('json'), json.stats);
app.get('/job/search', provides('json'), json.search);
app.get('/jobs/:from..:to/:order?', provides('json'), json.jobRange);
app.get('/jobs/:type/:state/:from..:to/:order?', provides('json'), json.jobTypeRange); // eslint-disable-line max-len
app.get('/jobs/:type/:state/stats', provides('json'), json.jobTypeStateStats);
app.get('/jobs/:state/:from..:to/:order?', provides('json'), json.jobStateRange); // eslint-disable-line max-len
app.get('/job/types', provides('json'), json.types);
app.get('/job/:id', provides('json'), json.job);
app.get('/job/:id/log', provides('json'), json.log);
app.put('/job/:id/state/:state', provides('json'), json.updateState);
app.put('/job/:id/priority/:priority', provides('json'), json.updatePriority);
app.delete('/job/:id', provides('json'), json.remove);
app.post('/job', provides('json'), bodyParser.json(), json.createJob);
app.get('/inactive/:id', provides('json'), json.inactive);
// TODO: This should really be a post.
app.get('/replay/:id', provides('json'), json.replay);
app.post('/force', provides('json'), bodyParser.json(), json.forceCreateJob);
// this is very useful for diagnostics, but we shouldn't let it be live until
// the json routes are protected by authentication since it displays names and
// whatnot
// app.get('/map', json.displayMap);

// routes
app.get('/', routes.jobs('active'));

app.get('/active', routes.jobs('active'));
app.get('/inactive', routes.jobs('inactive'));
app.get('/failed', routes.jobs('failed'));
app.get('/complete', routes.jobs('complete'));
app.get('/delayed', routes.jobs('delayed'));
// TODO: update this to show jobs that have been replayed?
app.get('/replayed', routes.jobs('replayed'));


// auth
app.get('/login', routes.auth());
app.post('/login', bodyParser.json(), bodyParser.urlencoded({extended: true}), routes.postAuth()); // eslint-disable-line max-len
app.post('/validate', bodyParser.json(), bodyParser.urlencoded({extended: true}), routes.authToken()); // eslint-disable-line max-len
