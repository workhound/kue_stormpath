/*!
 * kue - http - routes
 * Copyright (c) 2011 LearnBoost <tj@learnboost.com>
 * MIT Licensed
 */

/**
 * Module dependencies.
 */

const Queue = require('../../kue');
const Job = require('../../queue/job');
const queue = Queue.createQueue();
const request = require('request-promise');

/**
 * Serve the index page.
 */

exports.jobs = function( state ) {
  return function( req, res ) {
    queue.types(function( err, types ) {
      res.render('job/list', {
        state: state, types: types, title: req.app.get('title'),
      });
    });
  };
};

/**
 * Serve the login page
 */

exports.auth = function() {
    return (req, res) => {
        return res.render('auth/login');
    };
};

exports.postAuth = function() {
    return (req, res) => {
        const gatewayUrl = process.env.WORKHOUND_GATEWAY_URL;
        const headers = {
            origin: process.env.WORKHOUND_SERVER_ORIGIN,
        };
        const authRequest = {
            method: 'POST',
            uri: `${gatewayUrl}/authzero/login`,
            body: req.body,
            json: true,
            headers: headers,
        };

        request(authRequest)
        .then((response) => {
            try {
                return res.json({token: response.token});
            } catch (e) {
                throw new Error('No token included in response');
            }
        })
        .catch((err) => {
            let errorMsg = 'Oops... something went wrong';
            if (err.message.indexOf('401') > -1) {
                errorMsg = 'Invalid email/password combination';
            }
            console.error('ERROR authenticating user');
            console.error(err.message);
            res.json({error: errorMsg});
        });
    };
};

exports.authToken = function() {
    return (req, res) => {
        const gatewayUrl = process.env.WORKHOUND_GATEWAY_URL;
        const headers = {
            origin: process.env.WORKHOUND_SERVER_ORIGIN,
        };
        const authRequest = {
            method: 'POST',
            uri: `${gatewayUrl}/authzero/validate`,
            body: req.body,
            json: true,
            headers: headers,
        };

        request(authRequest)
        .then((response) => {
            res.json({result: 'ok', error: false, src: 'validation'});
        })
        .catch((err) => {
            res.json({error: true, src: 'validation'});
        });
    };
};
