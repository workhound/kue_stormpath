let env = process.env.NODE_ENV || 'development';
const helpers = require('./helpers');
const moment = require('moment');
const jwt = require('jsonwebtoken');

/**
 * Jams the x-wh-user header JSON string on the the req.user object where the
 * existing code expects it to be
 * @param  {Object}   req  [express request obeject]
 * @param  {Object}   res  [express response object]
 * @param  {Function} next [express middleware next()]
 */
module.exports = {
    headerToReq: function (req, res, next) {
        try {
            if (req.query.src !== 'hydra') {
                let user = JSON.parse(req.get('x-wh-user'));
                user = helpers.transformUserData(user);
                user = helpers.matchUserTenant(req, user);
                req.user = user;
            }
            next();
        } catch(e) {
            console.error('Error retrieving user from request header', e);
            next();
        }
    },

    checkJwt: function() {
        const whMiddleware = this;
        return function(req, res, next) {
            if (env === 'test') {
                return next();
            }

            const authHeader = req.get('authorization');
            if (!authHeader) {
                console.error('no auth header');
                return res.status(401).send({error: 'No authentication header'}); // eslint-disable-line max-len
            }

            const token = authHeader.split(' ')[1];
            if (!token) {
                console.error('no auth token');
                return res.status(401).send({error: 'No authentiaction token'}); // eslint-disable-line max-len
            }

            jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
              if (err) {
                console.error('ERROR authenticating token');
                return res.status(401).send({error: 'Failed to authenticate token.'}); // eslint-disable-line max-len
              } else {
                let valid = whMiddleware.validateToken(decoded);
                if (!valid) {
                    console.error('Token was expired');
                    return res.status(401).send({error: 'Token was expired'}); // eslint-disable-line max-len
                }
                return next();
              }
            });
        };
    },

    validateToken: function(decoded) {
        const tokenExpiry = process.env.AUTH0_TOKEN_EXPIRY;
        const now = moment();
        const expiresAt = moment(decoded.iat * 1000).add(tokenExpiry, 'ms');
        return !expiresAt.isSameOrBefore(now);
    },
};
