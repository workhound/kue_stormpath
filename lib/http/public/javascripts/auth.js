function Auth (user) {
    this.user = user;
    this.sendAuthRequest(this.user);
}

Auth.prototype.sendAuthRequest = function () {
    var validated = this.validateUser();
    if (validated) {
        toggleLoginLoading();
        request('POST', './login', this.resultHandler, this.user);
    }
}

Auth.prototype.validateUser = function () {
    var email = false;
    var pw = false;
    if (this.user.hasOwnProperty('email') && this.user.email !== null && this.user.email !== ''){
        email = true;
    }
    if (this.user.hasOwnProperty('password') && this.user.password !== null && this.user.password !== ''){
        pw = true;
    }
    return email && pw;
}


Auth.prototype.resultHandler = function (data) {
    if (data.token) {
        localStorage.setItem('id_token', data.token);
    }
    window.location = 'active';
}
